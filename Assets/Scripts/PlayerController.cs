﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Boundary
{
    public float zMin, zMax;
}

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float bounceSpeed;
    public Boundary boundary;
    private Rigidbody rigidBody;
    private float previousPosY;
    private float direction;

    public void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        previousPosY = 0;
    }

    void FixedUpdate ()
    {
        float moveVertical = Input.GetAxis("Vertical") * moveSpeed;

        if (transform.position.z + moveVertical > boundary.zMax)
        {
            var newPosition = transform.position;
            newPosition.z = boundary.zMax;
            transform.position = newPosition;
        }
        else if (transform.position.z + moveVertical < boundary.zMin)
        {
            var newPosition = transform.position;
            newPosition.z = boundary.zMin;
            transform.position = newPosition;
        }
        else
        {
            Vector3 movement = new Vector3(0.0f, 0.0f, moveVertical);
            transform.Translate(movement);
        }

        if (moveVertical != 0)
        {
            direction = Math.Sign(moveVertical);
        }
        else
        {
            direction = 0;
        }
        //var zPos = rigidBody.position.z;
        //zPos = Mathf.Clamp(zPos, boundary.zMin, boundary.zMax);
        //rigidBody.position = new Vector3(0.0f, 0.0f, zPos);
        //rigidBody.AddForce(new Vector3(0, 0, moveInput) * speed);
    }

    void LateUpdate()
    {
        previousPosY = transform.position.y;
    }

    public void OnCollisionExit(Collision collision)
    {
        float newVelocityY = bounceSpeed * direction;
        var oldVelocity = collision.rigidbody.velocity;
        collision.rigidbody.velocity = new Vector3(oldVelocity.x, 0.0f, oldVelocity.z + newVelocityY);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    public float startForce;
    
	void Start ()
    {
        GetComponent<Rigidbody>().AddForce(new Vector3(startForce, 0, 0));
	}
	
}
